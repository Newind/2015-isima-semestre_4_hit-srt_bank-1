<?php

namespace Bank\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bank\ProjectBundle\Entity\AccountRepository")
 */
class Account
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="accountLabel", type="string", length=255)
     */
    private $accountLabel;

    /**
     * @var integer
     *
     * @ORM\Column(name="currentMoney", type="integer")
     */
    private $currentMoney;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="accounts")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     **/
    private $owner;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountLabel
     *
     * @param string $accountLabel
     * @return Account
     */
    public function setAccountLabel($accountLabel)
    {
        $this->accountLabel = $accountLabel;

        return $this;
    }

    /**
     * Get accountLabel
     *
     * @return string 
     */
    public function getAccountLabel()
    {
        return $this->accountLabel;
    }

    /**
     * Set currentMoney
     *
     * @param integer $currentMoney
     * @return Account
     */
    public function setCurrentMoney($currentMoney)
    {
        $this->currentMoney = $currentMoney;

        return $this;
    }

    /**
     * Get currentMoney
     *
     * @return integer 
     */
    public function getCurrentMoney()
    {
        return $this->currentMoney;
    }

    /**
     * Set owner
     *
     * @param \Bank\ProjectBundle\Entity\User $owner
     * @return Account
     */
    public function setOwner(\Bank\ProjectBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Bank\ProjectBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
