<?php

namespace Bank\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Bank\ProjectBundle\Entity\Account;
use Bank\ProjectBundle\Form\AccountType;

/**
 * Account controller.
 *
 */
class AccountController extends Controller
{

    /**
     * Lists all Account entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $entities = $em->getRepository('BankProjectBundle:Account')->findByOwner($user->getId());

        return $this->render('BankProjectBundle:Account:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Account entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BankProjectBundle:Account')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $transactions = $em->getRepository('BankProjectBundle:Transaction')->getTransactionsFromAccount($entity)->getResult();

        return $this->render('BankProjectBundle:Account:show.html.twig', array(
            'entity'      => $entity,
            'transactions' => $transactions,
        ));
    }

    public function showBankStatementAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BankProjectBundle:Account')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $begin = \DateTime::createFromFormat('Y-m-d H:i:s', '2015-06-01 00:00:00');
        $end = \DateTime::createFromFormat('Y-m-d H:i:s', '2015-06-20 23:59:59');

        $transactions = $em->getRepository('BankProjectBundle:Transaction')->getTransactionsFromAccountAndInterval($entity, $begin, $end)->getResult();

        return $this->render('BankProjectBundle:Account:show.html.twig', array(
            'entity'      => $entity,
            'transactions' => $transactions,
        ));
    }

    public function showBankStatementPDFAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BankProjectBundle:Account')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $begin = \DateTime::createFromFormat('Y-m-d H:i:s', '2015-06-01 00:00:00');
        $end = \DateTime::createFromFormat('Y-m-d H:i:s', '2015-06-20 23:59:59');

        $transactions = $em->getRepository('BankProjectBundle:Transaction')->getTransactionsFromAccountAndInterval($entity, $begin, $end)->getResult();

        $html = $this->renderView('BankProjectBundle:Account:show.html.twig', array(
            'entity'      => $entity,
            'transactions' => $transactions,
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="file.pdf"'
            )
        );
    }



}
